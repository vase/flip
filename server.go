package flip

import (
	"log"
	"net"
)

type Server struct {
	listener    net.Listener
	newConns    chan net.Conn
	newCommands chan ClientWrappedCommand

	Store Storer
}

func NewServer(ln net.Listener) *Server {
	return &Server{
		listener:    ln,
		newConns:    make(chan net.Conn),
		newCommands: make(chan ClientWrappedCommand),
		Store:       NewMemoryStore(),
	}
}

func (s *Server) AcceptConnection() error {
	for {
		c, err := s.listener.Accept()
		if err != nil {
			s.newConns <- nil
			return err
		}
		s.newConns <- c
	}
}

func (s *Server) Run() {
	for {
		select {
		case c := <-s.newConns:
			if c != nil {
				log.Println("accepting new client connection")
				client := NewClient(c)
				go client.Read(s.newCommands)
			}
		case cmd := <-s.newCommands:
			if cmd.Error != nil {
				cmd.WriteError()
				continue
			}
			switch cmd.Type {
			case "INDEX":
				err := s.Store.Index(cmd.Package, cmd.PackageDeps...)
				if err != nil {
					log.Printf("store error: %s\n", err.Error())
					cmd.WriteFail()
				} else {
					cmd.WriteOK()
				}
			case "QUERY":
				exists, err := s.Store.Query(cmd.Package)
				if err != nil {
					log.Printf("store error: %s\n", err.Error())
					cmd.WriteFail()
				} else {
					if !exists {
						cmd.WriteFail()
					} else {
						cmd.WriteOK()
					}
				}
			case "REMOVE":
				removed, err := s.Store.Remove(cmd.Package)
				if err != nil {
					log.Printf("store error: %s\n", err.Error())
					cmd.WriteFail()
				} else {
					if !removed {
						cmd.WriteFail()
					} else {
						cmd.WriteOK()
					}
				}
			}
		}
	}
}
