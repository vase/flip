# flip


[![pipeline status](https://gitlab.com/vase/flip/badges/master/pipeline.svg)](https://gitlab.com/vase/flip/commits/master)

## Fake Little Indexer of Packages

`flip` is a toy package indexer for the DigitalOcean take-home coding assignment.

<br/>

## Getting Started

This project uses [`dep`](https://github.com/golang/dep) for dependency management. It only depends on one external package, for assertions during testing.

Install dependencies of `flip`:

```
$ dep ensure
```

You can then run `flip` from source:

```
$ go run server/main.go
```

The repository also contains a Dockerfile for building an Ubuntu 16.04 Docker image with `flip` installed/running:

```
$ docker build -t myrepo/flip .
```

You can then run the Docker container you just built:

```
$ docker run -it -d -p 8080:8080 myrepo/flip
```

<br/>

## Usage

`flip` responds to three different command types:

- `INDEX`: Index a given package
- `QUERY`: Query if a given package is indexed
- `REMOVE`: Remove an indexed package

Each command type is described in a little more detail below:

If any of the commands are not formatted correctly or invalid, the server will return with `ERROR`.

### INDEX

`INDEX|<package>|<dependencies>\n`

- `<package>` is the package that you are indexing
- `<dependencies>` is a comma-separated list of already-indexed packages that `<package>` depends on

If the specified package is already indexed, this command will re-index it with updated dependencies.
If the dependencies are not already indexed, this command will return with `FAIL`.

### QUERY

`QUERY|<package>|\n`

- `<package>` is the package that you are querying

If the specified package is indexed, this command will return with `OK`.
If the specified package is not indexed, this command will return with `FAIL`.

### REMOVE

`REMOVE|<package>|\n`

- `<package>` is the package that you are removing

If the specified package is not indexed, this command will return with `OK`.
If the specified package is a dependency of other packages that are indexed, this command will return with `FAIL`.

<br/>

## Design

The default storage mechanism of a command is a `MemoryStore`. This is a simple in-memory state of all indexed packages, what they depend on, and what depends on them. Since it is strictly in-memory, if the server is lost or rebooted, the state is lost.

Each `Package` object consists of three parts:

1. The name of the package
2. Other packages that it depends on
3. Other packages that depend on it

This mapping is convenient for mixed read/write workloads of a finite number of packages, and suffices for providing data relationships without a relational datastore.

Since the internals of `Package` are specific to the state of each `Package`, and direct modification can result in a bad state, the `Package` object uses getter and setter methods for retrieving and mutating its components.

<br/>

## Continuous Integration

There is a `.gitlab-ci.yml` file within the repository that contains the Continuous Integration steps. At the end of the pipeline, you have with a Docker image in the GitLab container registry (or a registry of your choice, in `$REGISTRY`) which runs a `flip` server that has undergone both unit and integration tests.
