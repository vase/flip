package main

import (
	"gitlab.com/vase/flip"
	"log"
	"net"
)

func main() {
	log.Println("flip starting...")

	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Printf("error starting tcp listener: %s\n", err.Error())
		return
	}
	defer ln.Close()

	server := flip.NewServer(ln)

	go func(s *flip.Server) {
		for {
			err := s.AcceptConnection()
			if err != nil {
				log.Println("closing acceptor")
				return
			}
		}
	}(server)

	server.Run()
}
