FROM golang:1.9.2

WORKDIR /go/src/gitlab.com/vase/flip/

COPY . .

RUN CGO_ENABLED=1 GOOS=linux go build -race -o flip server/main.go



FROM ubuntu:16.04

COPY --from=0 /go/src/gitlab.com/vase/flip/flip .

ENTRYPOINT ["/flip"]
