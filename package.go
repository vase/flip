package flip

// a Package is a struct representation of the actual
// objects that flip is keeping an index of.
type Package struct {
	// Name is the name of this package.
	Name string

	// dependsOn is a slice of package names that this
	// package depends on.
	dependsOn []string

	// isDependedOnBy is a slice of package names that
	// this package is depended on by.
	isDependedOnBy []string
}

func NewPackage(name string, dependsOn ...string) *Package {
	deps := make([]string, 0, len(dependsOn))
	for _, dep := range dependsOn {
		deps = append(deps, dep)
	}

	return &Package{
		Name:           name,
		dependsOn:      deps,
		isDependedOnBy: make([]string, 0),
	}
}

func (p *Package) DependsOn(pkgName string) bool {
	for _, dep := range p.dependsOn {
		if dep == pkgName {
			return true
		}
	}
	return false
}

func (p *Package) GetDependsOn() []string {
	return p.dependsOn
}

func (p *Package) IsDependedOnBy(pkgName string) bool {
	for _, dep := range p.isDependedOnBy {
		if dep == pkgName {
			return true
		}
	}
	return false
}

func (p *Package) GetIsDependedOnBy() []string {
	return p.isDependedOnBy
}

func (p *Package) AddIsDependedOnBy(newDep string) {
	for _, dep := range p.isDependedOnBy {
		if dep == newDep {
			return
		}
	}
	p.isDependedOnBy = append(p.isDependedOnBy, newDep)
}

func (p *Package) RemoveIsDependedOnBy(remDep string) {
	if len(p.isDependedOnBy) > 0 {
		newIsDependedOnBy := make([]string, 0, len(p.isDependedOnBy)-1)
		for _, dep := range p.isDependedOnBy {
			if dep != remDep {
				newIsDependedOnBy = append(newIsDependedOnBy, dep)
			}
		}
		p.isDependedOnBy = newIsDependedOnBy
	}
}
