package flip

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCommandWithValidCommandNoDeps(t *testing.T) {
	validCommand := "QUERY|examplepkg|\n"

	cmd, err := ParseCommand(validCommand)
	assert.Nil(t, err)
	assert.Equal(t, "QUERY", cmd.Type)
	assert.Equal(t, "examplepkg", cmd.Package)
	assert.Empty(t, cmd.PackageDeps)
}

func TestParseCommandWithValidCommand(t *testing.T) {
	validCommand := "QUERY|examplepkg|dep1,dep2\n"

	cmd, err := ParseCommand(validCommand)
	assert.Nil(t, err)
	assert.Equal(t, "QUERY", cmd.Type)
	assert.Equal(t, "examplepkg", cmd.Package)
	assert.Equal(t, "dep1", cmd.PackageDeps[0])
	assert.Equal(t, "dep2", cmd.PackageDeps[1])
}

func TestParseCommandWithInvalidType(t *testing.T) {
	invalidCommand := "FAKE|examplepkg|\n"

	_, err := ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrInvalidCommand, err)
}

func TestParseCommandWithMissingType(t *testing.T) {
	invalidCommand := "|examplepkg|\n"

	_, err := ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrInvalidCommand, err)
}

func TestParseCommandWithMissingPackage(t *testing.T) {
	invalidCommand := "INDEX||\n"

	_, err := ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrInvalidCommand, err)
}

func TestParseCommandWithMalformedCommand(t *testing.T) {
	invalidCommand := "malformed\n"

	_, err := ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrMalformedCommand, err)
}

func TestParseCommandWithBadPackage(t *testing.T) {
	invalidCommand := "INDEX|A! B|\n"

	_, err := ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrInvalidCommand, err)

	invalidCommand = "INDEX|\"\"|\n"

	_, err = ParseCommand(invalidCommand)
	assert.NotNil(t, err)
	assert.Equal(t, ErrInvalidCommand, err)
}
