package flip

type Storer interface {
	Index(pkgName string, dependsOn ...string) error
	Query(pkgName string) (bool, error)
	Remove(pkgName string) (bool, error)
}
