package flip

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetDependsOn(t *testing.T) {
	p := NewPackage("examplepkg", "dependedOnPackage")

	deps := p.GetDependsOn()
	assert.Equal(t, 1, len(deps))
	assert.Equal(t, "dependedOnPackage", deps[0])
}

func TestDependsOn(t *testing.T) {
	dep := "dependedOnPackage"

	// p "depends on" package dep
	p := NewPackage("examplepkg", dep)

	assert.True(t, p.DependsOn(dep))
	assert.False(t, p.DependsOn("notADependedOnPackage"))
}

func TestGetIsDependedOnBy(t *testing.T) {
	p := NewPackage("examplepkg")
	p.isDependedOnBy = append(p.isDependedOnBy, "dependentPackage")

	deps := p.GetIsDependedOnBy()

	assert.Equal(t, 1, len(deps))
	assert.Equal(t, "dependentPackage", deps[0])
}

func TestIsDependedOnBy(t *testing.T) {
	dep := "dependentPackage"

	p := NewPackage("examplepkg")

	p.isDependedOnBy = append(p.isDependedOnBy, "dependentPackage")

	assert.True(t, p.IsDependedOnBy(dep))
	assert.False(t, p.IsDependedOnBy("notADependentPackage"))
}

func TestAddIsDependedOnBy(t *testing.T) {
	p := NewPackage("examplepkg")
	p.AddIsDependedOnBy("dependentPackage")

	assert.Equal(t, 1, len(p.isDependedOnBy))
	assert.Equal(t, "dependentPackage", p.isDependedOnBy[0])
}

func TestRemoveIsDependedOnBy(t *testing.T) {
	p := NewPackage("examplepkg")
	p.isDependedOnBy = append(p.isDependedOnBy, "dependentPackage")

	oldDeps := p.GetIsDependedOnBy()
	p.RemoveIsDependedOnBy("dependentPackage")
	newDeps := p.GetIsDependedOnBy()

	assert.Equal(t, 0, len(p.isDependedOnBy))
	assert.False(t, assert.ObjectsAreEqual(oldDeps, newDeps))
}

func TestRemoveIsDependedOnByWhenEmpty(t *testing.T) {
	p := NewPackage("examplepkg")

	oldDeps := p.GetIsDependedOnBy()
	p.RemoveIsDependedOnBy("notadependency")
	newDeps := p.GetIsDependedOnBy()

	assert.True(t, assert.ObjectsAreEqual(oldDeps, newDeps))
}
