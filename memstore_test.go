package flip

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var memstore *MemoryStore

func setup() {
	memstore = NewMemoryStore()
}

func TestIndexNoDependencies(t *testing.T) {
	setup()

	err := memstore.Index("examplepkg")

	assert.Nil(t, err)

	_, ok := memstore.indexed["examplepkg"]
	assert.True(t, ok)
}

func TestIndexMissingDependencies(t *testing.T) {
	setup()

	err := memstore.Index("examplepkg", "missingdep")

	assert.NotNil(t, err)
	_, ok := memstore.indexed["examplepkg"]
	assert.False(t, ok)
}

func TestIndexWithDependencies(t *testing.T) {
	setup()

	memstore.Index("depA")
	memstore.Index("depB")
	err := memstore.Index("examplepkg", "depA", "depB")

	assert.Nil(t, err)
	_, ok := memstore.indexed["examplepkg"]
	assert.True(t, ok)
}

func TestIndexAlreadyIndexed(t *testing.T) {
	setup()

	memstore.Index("depA")
	memstore.Index("examplepkg", "depA")

	memstore.Index("depB")
	err := memstore.Index("examplepkg", "depB")

	assert.Nil(t, err)
	pkg, ok := memstore.indexed["examplepkg"]
	assert.True(t, ok)

	deps := pkg.GetDependsOn()
	assert.Contains(t, deps, "depB")
	assert.NotContains(t, deps, "depA")
}

func TestQueryIndexedPackage(t *testing.T) {
	setup()

	memstore.Index("examplepkg")
	indexed, err := memstore.Query("examplepkg")

	assert.Nil(t, err)
	assert.True(t, indexed)
}

func TestQueryMissingPackage(t *testing.T) {
	setup()

	indexed, err := memstore.Query("examplepkg")

	assert.Nil(t, err)
	assert.False(t, indexed)
}

func TestRemoveIndexedPackage(t *testing.T) {
	setup()

	memstore.Index("examplepkg")

	removed, err := memstore.Remove("examplepkg")

	assert.Nil(t, err)
	assert.True(t, removed)
}

func TestRemoveMissingPackage(t *testing.T) {
	setup()

	removed, err := memstore.Remove("examplepkg")

	assert.Nil(t, err)
	assert.True(t, removed)
}

func TestRemoveDependedOnPackage(t *testing.T) {
	setup()

	memstore.Index("depA")
	memstore.Index("examplepkg", "depA")

	removed, err := memstore.Remove("depA")

	assert.Nil(t, err)
	assert.False(t, removed)
}

func TestRemoveDependentPackage(t *testing.T) {
	setup()

	memstore.Index("depA")
	memstore.Index("examplepkg", "depA")

	removed, err := memstore.Remove("examplepkg")

	assert.Nil(t, err)
	assert.True(t, removed)
}
