package flip

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

// a MemoryStore is an in-memory store of the current
// state of indexed packages. It uses a stdlib map for storage,
// which is not inherently thread-safe in Go. However, the
// MemoryStore also stores a mutex for locking to prevent
// data races on the internal indexed map.
type MemoryStore struct {
	// mutex for locking when reading or writing to indexed map
	*sync.RWMutex

	// indexed contains the internal list of packages. The key
	// is the package name, and the value is a pointer to the
	// package object itself.
	indexed map[string]*Package
}

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		RWMutex: &sync.RWMutex{},
		indexed: map[string]*Package{},
	}
}

// implements the Storer interface
func (s *MemoryStore) Index(pkgName string, dependsOn ...string) error {
	s.Lock()
	defer s.Unlock()

	missingDeps := make([]string, 0)
	for _, dep := range dependsOn {
		if pkgName == dep {
			// the package to be indexed has a dependency on itself
			errorMsg := fmt.Sprintf("error indexing package %s: dependency on self", pkgName)
			return errors.New(errorMsg)
		}
		if !s.exists(dep) {
			missingDeps = append(missingDeps, dep)
		}
	}

	if len(missingDeps) != 0 {
		errorMsg := fmt.Sprintf("error indexing package %s: missing dependencies (missing: %s)", pkgName, strings.Join(missingDeps, ","))
		return errors.New(errorMsg)
	}

	if s.exists(pkgName) {
		// before we index the package, if it already exists, we want
		// to make sure to update isDependedOnBy of packages that it
		// depends on
		oldPkg := s.indexed[pkgName]
		for _, oldDep := range oldPkg.GetDependsOn() {
			var stillDep bool
			for _, newDep := range dependsOn {
				if oldDep == newDep {
					stillDep = true
					break
				}
			}
			if !stillDep {
				s.indexed[oldDep].RemoveIsDependedOnBy(pkgName)
			}
		}
	}

	s.indexed[pkgName] = NewPackage(pkgName, dependsOn...)

	for _, dep := range dependsOn {
		//update list of "isDependedOnBy" values with pkgName
		s.indexed[dep].AddIsDependedOnBy(pkgName)
	}

	return nil
}

// implements the Storer interface
func (s *MemoryStore) Query(pkgName string) (bool, error) {
	s.RLock()
	defer s.RUnlock()
	if _, ok := s.indexed[pkgName]; !ok {
		return false, nil
	}
	return true, nil
}

// implements the Storer interface
func (s *MemoryStore) Remove(pkgName string) (bool, error) {
	s.Lock()
	defer s.Unlock()

	if !s.exists(pkgName) {
		// return OK if the package is not indexed
		return true, nil
	}

	// if the indexed package is depended on by any other packages,
	// then it cannot be removed
	if len(s.indexed[pkgName].GetIsDependedOnBy()) != 0 {
		return false, nil
	}

	// for all the packages that pkgName depends on, we want to make
	// sure that their isDependedOnBy is updated to remove pkgName
	for _, dep := range s.indexed[pkgName].GetDependsOn() {
		if s.indexed[dep] != nil && len(s.indexed[dep].GetIsDependedOnBy()) > 0 {
			s.indexed[dep].RemoveIsDependedOnBy(pkgName)
		}
	}

	delete(s.indexed, pkgName)
	return true, nil
}

func (s *MemoryStore) exists(pkgName string) bool {
	if _, ok := s.indexed[pkgName]; !ok {
		return false
	}
	return true
}
