package flip

import (
	"errors"
	"regexp"
	"strings"
)

var (
	ErrMalformedCommand = errors.New("error: malformed command")
	ErrInvalidCommand   = errors.New("error: command not valid")
)

// A Command is a struct representation of what was passed
// to the server from a client. It validates client-side data
// based on the spec in INSTRUCTIONS.md.
type Command struct {
	Type        string
	Package     string
	PackageDeps []string
}

// Validate checks to ensure that the command passed to the
// server follows the spec in INSTRUCTIONS.md. Namely:
//   - the first term is a (required) verb.
//   - the second term is a (required) package name.
//   - the third term is a(n) (optional) list of package names,
//     comma-separated.
func (c *Command) Validate() (ok bool) {
	ok = true

	switch c.Type {
	case "INDEX", "REMOVE", "QUERY":
		// these are valid
	default:
		ok = false
	}

	re := regexp.MustCompile("^[a-zA-Z0-9\\+\\-_]+$")
	matched := re.MatchString(c.Package)
	if !matched {
		ok = false
	}
	return
}

// ParseCommand takes the raw command that was passed to the
// server and parses it into a Command object, validating
// that it meets the spec defined in INSTRUCTIONS.md.
func ParseCommand(cmdStr string) (*Command, error) {
	cmd := &Command{}

	if strings.TrimSpace(cmdStr) == "" {
		return nil, ErrMalformedCommand
	}

	cmdSplit := strings.Split(cmdStr, "|")
	if len(cmdSplit) != 3 {
		return nil, ErrMalformedCommand
	}

	// trim off the trailing newline character from the last element of
	// the package dependencies list
	cmdDeps := strings.TrimSpace(cmdSplit[2])
	if cmdDeps != "" {
		deps := strings.Split(cmdDeps, ",")
		cmd.PackageDeps = deps
	} else {
		cmd.PackageDeps = make([]string, 0)
	}

	cmd.Type = cmdSplit[0]
	cmd.Package = cmdSplit[1]

	if ok := cmd.Validate(); !ok {
		return nil, ErrInvalidCommand
	}

	return cmd, nil
}
