package flip

import (
	"bufio"
	"io"
	"log"
	"net"
	"strings"
)

type Client struct {
	reader *bufio.Reader
	writer *bufio.Writer
	conn   net.Conn
}

type ClientWrappedCommand struct {
	*Command
	*Client
	Error error
}

const (
	OK    = "OK\n"
	FAIL  = "FAIL\n"
	ERROR = "ERROR\n"
)

func NewClient(connection net.Conn) *Client {
	return &Client{
		reader: bufio.NewReader(connection),
		writer: bufio.NewWriter(connection),
		conn:   connection,
	}
}

func (c *Client) Read(outgoing chan ClientWrappedCommand) {
	for {
		message, err := c.reader.ReadString('\n')
		if err != nil && err == io.EOF {
			c.conn.Close()
			log.Println("client connection closed")
			return
		}

		cmd, err := ParseCommand(string(message))
		if err != nil {
			log.Printf("%s: (%s)\n", err.Error(), string(strings.TrimSpace(message)))
			outgoing <- ClientWrappedCommand{nil, c, err}
			continue
		}
		outgoing <- ClientWrappedCommand{cmd, c, nil}
	}
}

func (c *Client) write(message string) {
	c.writer.WriteString(message)
	c.writer.Flush()
}

func (c *Client) WriteOK() {
	c.write(OK)
}

func (c *Client) WriteFail() {
	c.write(FAIL)
}

func (c *Client) WriteError() {
	c.write(ERROR)
}
